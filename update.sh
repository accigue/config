#!/bin/bash

DOWNLOAD_SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/master/download.sh"
UPDATE_SCRIPT_URL="https://gitlab.com/wingbits/config/-/raw/master/update.sh"
VECTOR_SERVICE_PATH="/lib/systemd/system/vector.service"
CRON_IDENTIFIER="wingbits_config_update"
LOG_FILE="/var/log/wingbits/wb_update.log"

# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root"
    echo "Run it like this:"
    echo "sudo ./update.sh"
    exit 1
fi

local_version=$(cat /etc/wingbits/version)
script=$(curl -s $DOWNLOAD_SCRIPT_URL)
version=$(echo "$script" | grep -oP '(?<=WINGBITS_CONFIG_VERSION=")[^"]*')

version_major_minor=$(echo "$version" | cut -d'.' -f1-2)
local_version_major_minor=$(echo "$local_version" | cut -d'.' -f1-2)

echo "--------------------------"
echo "$(date)"
echo "Current local version: $local_version"
echo "Latest available Wingbits version: $version"

### handle cron frequency updates ###
# Remove the old cron job with the unique identifier, if it exists
if crontab -l | grep -q "$CRON_IDENTIFIER"; then
    (crontab -l | grep -v "$CRON_IDENTIFIER") | crontab -
fi

# Add the new cron job
mkdir -p /var/log/wingbits
UPDATE_JOB="0 18 * * * /usr/bin/curl -s $UPDATE_SCRIPT_URL | /bin/bash >> $LOG_FILE 2>&1 # $CRON_IDENTIFIER"
(crontab -l ; echo "$UPDATE_JOB") | crontab -

# If the vector config file is missing or 0 bytes run the script to correct
if [[ ! -e /etc/vector/vector.yaml || ! -s /etc/vector/vector.yaml ]]; then
	
    curl -sL https://gitlab.com/wingbits/config/-/raw/master/fix_yaml.sh | bash
	
fi

# Update/add restartsec option to vector service if needed
wb_restart_sec=180
if [[ -e /lib/systemd/system/vector.service ]]; then
    # if the service file is not already setup with the appropriate line
    if ! grep -q "^RestartSec=$wb_restart_sec" "$VECTOR_SERVICE_PATH"; then
	vector_edit_mode=''
        # see if it doesn't have the line at all, if so add it
        if ! grep -q "^RestartSec=" "$VECTOR_SERVICE_PATH"; then
            # Insert above the line containing 'StartLimitInterval='
            sed -i "/StartLimitInterval/i RestartSec=$wb_restart_sec" "$VECTOR_SERVICE_PATH"
	    vector_edit_mode='added'
        else # otherwise update the current value
            sed -i "s/RestartSec=.*/RestartSec=$wb_restart_sec/" "$VECTOR_SERVICE_PATH"
	    vector_edit_mode='updated'
	fi

        echo "vector ($vector_edit_mode): set RestartSec=$wb_restart_sec"
	
        # for either case above, we need to reload systemd manager and restart vector
        systemctl daemon-reload
        systemctl restart vector.service

        echo "systemd manager reloaded and vector restarted."
    fi
    # if it was already set properly, nothing else to do
fi
