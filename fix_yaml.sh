#!/bin/bash

# Throw warning if script is not executed as root
if [[ $EUID -ne 0 ]]; then
    echo "ERROR: This script must be run as root via sudo"
    exit 1
fi

echo "Downloading latest vector configuration for Wingbits..."
mkdir -p /etc/vector
curl -o /etc/vector/vector.yaml "https://gitlab.com/wingbits/config/-/raw/master/vector.yaml"

# add/replace vector env var file
device_id=$(< /etc/wingbits/device)
echo "DEVICE_ID=\"$device_id\"" > /etc/default/vector
chmod 644 /etc/default/vector

# start/restart vector
echo ""
echo "Starting/restarting vector..."
systemctl restart vector

echo ""
echo "If errors are shown from restarting vector, please add the output of this script in your ticket"

